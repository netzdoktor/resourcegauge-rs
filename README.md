<!-- 
SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>

SPDX-License-Identifier: MIT
-->

<img src="docs/logo/resourcegauge-rs.svg" alt="resourcegauge-rs" />

a toolkit for ergonomic and dependable resource-aware software in Rust

[![Crates.io](https://img.shields.io/crates/l/resourcegauge.svg)](https://crates.io/crates/resourcegauge)
[![Crates.io](https://img.shields.io/crates/v/resourcegauge.svg)](https://crates.io/crates/resourcegauge)
[![Documentation](https://docs.rs/resourcegauge/badge.svg)](https://docs.rs/resourcegauge)
[![Minimum Stable Rust Version](https://img.shields.io/badge/Rust-nightly-blue?color=fc8d62&logo=rust)](https://rustup.rs/)
<!--[![ClearlyDefined Score](https://img.shields.io/clearlydefined/score/crate/cratesio/-/resourcegauge/0.0.0?label=ClearlyDefined%20Score)](https://clearlydefined.io/definitions/crate/cratesio/-/resourcegauge/0.0.0)-->
[![REUSE status](https://api.reuse.software/badge/gitlab.com/netzdoktor/resourcegauge-rs)](https://api.reuse.software/info/gitlab.com/netzdoktor/resourcegauge-rs)
[![Dependency Status](https://deps.rs/repo/gitlab/netzdoktor/resourcegauge-rs/status.svg)](https://deps.rs/repo/gitlab/netzdoktor/resourcegauge-rs)
<!--[![Build Status](https://img.shields.io/github/workflow/status/netzdoktor/resourcegauge/Run%20CI/main)](https://github.com)-->

The goal of this library is to provide macros and types to make computation resources (such as energy and latency) first-class citizens.

<!-- Similar projects, such as [EnerJ](https://sampa.cs.washington.edu/research/approximation/enerj.html) -->

**The end goal of this project is to ergonomically allow to measure, optimize, and enforce resource budgets (using both compile- and runtime analysis).**

## Functionality

* At compile-time:
  * Statically check composability of resource budgets (via zero-sized, const generic types)
  * Enforce resource-error handling (via  `Result`)
  * Provide resource-aware data types (e.g. `ExpiringAt`)
  * Add resource-awareness runtime code (via procedural macros)
* At run-time
  * Monitor and expose resource usage
  * Handle resource-related failures

## Example

Here is resourcegauge in use:

```rust
use resourcegauge::prelude::*;    // imports check!(), deadline!(), ResourceFailure

#[resourcegauge(max_latency="10sec")]
fn receive() -> SomeType  // transformed to -> Result<SomeType, ResourceFailure>
{
    // let TIMINGS = ...              <- inserted by macro; static & global 
    // let DEADLINE = ...             <- inserted by macro; invocation-local
    let s = socket(); // amenable to measurement (dynamic)
    check!();  // transformed to check for ResourceExhausted (now > deadline) or ResourceExhaustionExpected (now + rest > deadline)

    let d = s.recv_timeout(remaining_latency!()); // early deadline computed based on rest of function
    //         ^--- maybe resourcegauge can also replace std::net::socket:recv() with recv_timeout() automatically and insert deadline!()
    check!();
    let r = d[0..255].to_upper(); // amenable to WCET (static)
    // another check!()-like block is introduced by the function-macro (check for deadline, transform to Result)
}
```

## Usage

Start by adding the following to your top-level module:

```rust
#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(inline_const)]
#![feature(adt_const_params)]
#![feature(const_option)]
```

Afterwards, import `resourcegauge` wherever you need it:

```rust
use resourcegauge::prelude::*;
```

Finally, you can start modifing your function:

```rust
#[resourcegauge(max_latency="1s")] // <- add this 
fn f() -> bool {
    // ...
}
```

### Best Practise

* Use `with_latency` for statements that call other `#[resourcegauge]` functions.
* Ideally: Write functions with single return statement.

## Stability

Most features of this crate require a nightly Rust compiler (see [`rust-toolchain`](rust-toolchain)). We rely on the following unstable and incomplete features:

* [`adt_const_params`](https://github.com/rust-lang/rust/issues/95174)
* [`const_option`](https://github.com/rust-lang/rust/issues/67441),
* [`generic_const_exprs`](https://github.com/rust-lang/rust/issues/76560)
* [`inline_const`](https://github.com/rust-lang/rust/issues/76001)

## FAQ

### I need help!

Don't hesitate to file an issue or contact [@netzdoktor](https://gitlab.com/netzdoktor) via e-mail.

### How can I help?

Please have a look at the issues or open one if you feel that something is needed.

Any contributions are very welcome!

## License

Licensed under the MIT license ([LICENSE-MIT](./LICENSES/MIT.txt) or http://opensource.org/licenses/MIT).

## Acknowledgements

![CPEC](https://www.perspicuous-computing.science/wp-content/uploads/2019/11/Logo_CPEC_final_RGB.png)

This work was funded by the German Research Foundation (DFG) grant 389792660 as part of TRR&nbsp;248 &ndash; [CPEC](https://perspicuous-computing.science).

<!--## Citation

Cite this work as

```bibtex
@article{schmidt:2023:resourcegauge,
  title        = {Ergonomic and Dependable Resource-Aware Software Components in Rust},
  author       = {Schmidt, Andreas and Gerhorst, Luis and Vogelgesang, Kai},
  journal      = {TbA},
  year         = {2023}
}
```-->

## Contributing

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, shall be MIT licensed as above, without any additional terms or conditions.
