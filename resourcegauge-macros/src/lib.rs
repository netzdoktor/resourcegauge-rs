#![crate_type = "proc-macro"]

// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro_error::proc_macro_error;
use syn::{parse_macro_input, spanned::Spanned};

#[proc_macro_error]
#[proc_macro_attribute]
pub fn resourcegauge(args: TokenStream, item: TokenStream) -> TokenStream {
    let args: proc_macro2::TokenStream = args.into();
    let args_span = args.span();
    let args: proc_macro::TokenStream = args.into();
    let args = parse_macro_input!(args as syn::AttributeArgs);
    resourcegauge_core::main_macro(args, args_span, item.into()).into()
}

#[proc_macro_error]
#[proc_macro]
pub fn check(item: TokenStream) -> TokenStream {
    resourcegauge_core::check(item.into()).into()
}

#[proc_macro_error]
#[proc_macro]
pub fn remaining_latency(item: TokenStream) -> TokenStream {
    resourcegauge_core::remaining_latency(item.into()).into()
}

#[proc_macro_error]
#[proc_macro]
pub fn next_deadline(item: TokenStream) -> TokenStream {
    resourcegauge_core::next_deadline(item.into()).into()
}

#[proc_macro_error]
#[proc_macro]
pub fn with_latency(item: TokenStream) -> TokenStream {
    resourcegauge_core::with_latency(item.into()).into()
}
