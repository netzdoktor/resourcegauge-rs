// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

pub use crate::expiring::{ExpiringAt, ExpiringIn};
pub use crate::{FailureMode, ResourceBudget, ResourceFailure};
pub use resourcegauge_macros::{
    check, next_deadline, remaining_latency, resourcegauge, with_latency,
};
