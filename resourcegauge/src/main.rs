#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(inline_const)]
#![feature(adt_const_params)]
#![feature(const_option)]

// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use resourcegauge::prelude::*;
use std::time::Duration;

#[resourcegauge(max_latency = "2s")]
fn f1() -> bool {
    let _f = "Bar";
    check!();
    with_latency! {
        let _x = f2();
    };
    with_latency! {
        let _x = f3();
    };
    check!();
    let l = remaining_latency!();
    println!("Remaining Latency: {:#?}", l);
    let d = next_deadline!();
    println!("Next Deadline: {:#?}", d);
    check!();

    let _f = || {
        if true {
            #[allow(clippy::needless_return)]
            return 5;
        } else {
            7
        }
    };

    if false {
        return false;
    } else {
        std::thread::sleep(Duration::from_millis(650));
        std::thread::sleep(Duration::from_millis(40));
        true
    }
}

#[resourcegauge(max_latency = "1s")]
fn f2() {
    std::thread::sleep(Duration::from_millis(650));
    with_latency! {
        let _ = f3();
    };
    with_latency! {
        let _ = f3();
    };
    ()
}

#[resourcegauge(max_latency = "1s")]
fn f3() {
    std::thread::sleep(Duration::from_millis(650));
}

fn main() {
    let l = ResourceBudget::<{ Duration::from_secs(2) }>::new();
    println!("{:#?}", l.value());
    for _ in 0..5 {
        let l = ResourceBudget::<{ Duration::from_secs(2) }>::new();
        match f1(l) {
            Ok(_) => continue,
            Err(e) => {
                println!("Timeout: {:#?}", e)
            }
        }
    }
    println!("Done");
}
