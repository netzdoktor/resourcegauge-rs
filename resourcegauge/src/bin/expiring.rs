// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use resourcegauge::prelude::*;
use std::time::Duration;

#[allow(clippy::unwrap_used)]
fn expiring_data() {
    const D: Duration = Duration::from_millis(300);
    let d_static = ExpiringIn::<_, D>::from_now(5);
    println!("{}", d_static.take().unwrap());

    let d_static = ExpiringIn::<_, D>::from_now(5);

    let d_dyn1 = ExpiringAt::from_now(5, Duration::from_millis(300));
    let d_dyn2 = ExpiringAt::from_now(7, Duration::from_millis(500));
    let d_dyn3 = (d_dyn1 + d_dyn2) * d_dyn2 - d_dyn2 - d_static.into();
    std::thread::spawn(move || {
        println!("{}", d_dyn3.take().unwrap());
    });
}

fn main() {
    expiring_data()
}
