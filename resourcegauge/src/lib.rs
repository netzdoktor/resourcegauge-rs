#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(inline_const)]
#![feature(adt_const_params)]
#![feature(const_option)]

// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use std::marker::PhantomData;
use std::time::Duration;

pub mod expiring;
pub mod prelude;

#[derive(Debug, PartialEq)]
pub enum FailureMode<T> {
    BudgetExceeded(T),
    BudgetExceedanceExpected(T),
}

#[derive(Debug, PartialEq)]
pub enum ResourceFailure {
    Latency(FailureMode<std::time::Duration>),
    Energy(FailureMode<u32>),
}

// TODO: Energy
// pub struct Energy { joules: u128, nanojoules: u128 }
// impl Energy { /* Fundamental Math, Energy::now() and elapsed() on appropriate systems */}
// pub struct ResourceEnergy<const ENERGY: Energy> {}

pub struct ResourceBudget<const LATENCY: Duration> {
    _data: PhantomData<()>,
}

impl<const LATENCY: Duration> ResourceBudget<LATENCY> {
    pub const fn new() -> Self {
        Self {
            _data: PhantomData {},
        }
    }

    #[inline(always)]
    pub const fn shorten<const LATENCY2: Duration>(self) -> ResourceBudget<LATENCY2> {
        // ATTEMPT: const _ : Duration = LATENCY.checked_sub(LATENCY2).unwrap();
        // TODO: error condition only checked at runtime
        if LATENCY.checked_sub(LATENCY2).is_some() {
            ResourceBudget::new()
        } else {
            panic!("Latencies can only be shortened")
        }
    }

    #[inline(always)]
    pub const fn value(&self) -> std::time::Duration {
        LATENCY
    }
}

// TODO: example of a function where a ExpiringIn type is passed in; pass by reference should be disallowed -> move (and potentially return changed)
