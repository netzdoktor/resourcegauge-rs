// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use crate::{FailureMode, ResourceFailure};
use std::time::{Duration, Instant};

/// The ExpiringIn type allows to statically specify a expiration duration.
/// Thereby, static analysis is possible.
pub struct ExpiringIn<T, const D: Duration> {
    data: T,
    at: Instant,
}

impl<T: Copy, const D: Duration> Copy for ExpiringIn<T, D> {}
impl<T: Clone, const D: Duration> Clone for ExpiringIn<T, D> {
    fn clone(&self) -> Self {
        Self {
            data: self.data.clone(),
            at: self.at,
        }
    }
}

impl<T, const D: Duration> ExpiringIn<T, D> {
    pub fn from_now(data: T) -> Self {
        Self {
            data,
            at: Instant::now() + D,
        }
    }

    /// Allowing to check if the data is still valid before using it eventually.
    pub fn check(&self) -> Result<(), ResourceFailure> {
        if self.at > Instant::now() {
            Ok(())
        } else {
            Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(
                Instant::now() - self.at,
            )))
        }
    }

    /// ExpiringIn is consumed and the data returned if valid.
    /// Afterwards, the consumer must make sure that only valid data is processed.
    pub fn take(self) -> Result<T, ResourceFailure> {
        if self.at > Instant::now() {
            Ok(self.data)
        } else {
            Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(
                Instant::now() - self.at,
            )))
        }
    }
}

// TODO: allow borrow (bound to lifetime of ExpiringIn) but enforce compiler to use up / and drop ExpiringIn.
// TODO: smart pointer with Deref; on Drop, the validity is checked again
// TODO: borrow_mut -> SmartPointer<&T>; in Drop, validity is checked; .return();

/// The ExpiringAt type allows to dynamically specify a expiration instant.
/// Thereby, dynamic checks are possible.
pub struct ExpiringAt<T> {
    data: T,
    at: Instant,
}

impl<T, const D: Duration> From<ExpiringIn<T, D>> for ExpiringAt<T> {
    fn from(value: ExpiringIn<T, D>) -> Self {
        Self {
            data: value.data,
            at: value.at,
        }
    }
}

impl<T: Copy> Copy for ExpiringAt<T> {}
impl<T: Clone> Clone for ExpiringAt<T> {
    fn clone(&self) -> Self {
        Self {
            data: self.data.clone(),
            at: self.at,
        }
    }
}

impl<T> ExpiringAt<T> {
    pub fn from_now(data: T, duration: Duration) -> Self {
        Self {
            data,
            at: Instant::now() + duration,
        }
    }

    pub fn at(data: T, at: Instant) -> Self {
        Self { data, at }
    }

    /// Expiring is consumed and the data returned if valid.
    /// Afterwards, the consumer must make sure that only valid data is processed.
    pub fn take(self) -> Result<T, ResourceFailure> {
        if self.at > Instant::now() {
            Ok(self.data)
        } else {
            Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(
                Instant::now() - self.at,
            )))
        }
    }

    /// Allowing to check if the data is still valid before using it eventually.
    pub fn check(&self) -> Result<(), ResourceFailure> {
        if self.at > Instant::now() {
            Ok(())
        } else {
            Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(
                Instant::now() - self.at,
            )))
        }
    }

    /// Call given closure with self's data and wrap result in ExpiringAt with an identical deadline.
    pub fn map<U>(&self, closure: impl FnOnce(&T) -> U) -> ExpiringAt<U> {
        ExpiringAt {
            data: closure(&self.data),
            at: self.at,
        }
    }

    /// Call given closure with self's data and wrap result in ExpiringAt with an identical deadline.
    /// New ExpiringIn is consumed and the data is returned if valid.
    /// Afterwards, the consumer must make sure that only valid data is processed.
    /// Equivalent to chain map and take.
    pub fn with<U>(&self, closure: impl FnOnce(&T) -> U) -> Result<U, ResourceFailure> {
        self.map(closure).take()
    }
}

macro_rules! ops {
    ($ops:ident, $op:ident) => {
        impl<T, O, R> std::ops::$ops<ExpiringAt<R>> for ExpiringAt<T>
        where
            T: std::ops::$ops<R, Output = O>,
        {
            type Output = ExpiringAt<O>;

            fn $op(self, rhs: ExpiringAt<R>) -> Self::Output {
                ExpiringAt::<O> {
                    data: std::ops::$ops::$op(self.data, rhs.data),
                    at: self.at.min(rhs.at),
                }
            }
        }
    };
}

// Implementing Operator Traits using code generation
ops!(Add, add);
// TODO: AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Div, DivAssign,
ops!(Mul, mul);
// TODO: MulAssign, Net, Not, Rem, RemAssign, Residual, Shl, ShlAssign, Shr, ShrAssign
ops!(Sub, sub);
// TODO: SubAssign

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_numerical_types() {
        let d1 = ExpiringAt::from_now(5, Duration::from_millis(300));
        let d2 = ExpiringAt::from_now(7, Duration::from_millis(500));
        let d3 = ExpiringAt::from_now(7, Duration::from_millis(300));
        assert_eq!(d1.check(), Ok(()));
        assert_eq!(d2.check(), Ok(()));
        let d4 = d1 + d2;
        assert_eq!(d4.take(), Ok(12));
        std::thread::sleep(Duration::from_secs(1));

        if let Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(_))) = d3.take() {
        } else {
            panic!()
        }
    }

    #[test]
    fn test_different_rhs() {
        let t = Instant::now();
        let d1 = ExpiringAt::from_now(t, Duration::from_millis(100));
        let d2 = d1 + ExpiringAt::from_now(Duration::from_secs(1), Duration::from_millis(200));
        assert_eq!(d2.take().unwrap(), t + Duration::from_secs(1));
    }

    #[test]
    fn test_other_types() {
        enum Foo {
            A,
            B,
        }

        let _d1 = ExpiringAt::from_now(Foo::A, Duration::from_millis(100));
        let _d2 = ExpiringAt::from_now(Foo::B, Duration::from_millis(100));
        // does not work d1 + d2
    }

    #[test]
    fn test_with_valid() {
        let closure = |a: &u32| -> u32 { a * 2 };
        let expiring_at = ExpiringAt::from_now(2, Duration::from_millis(1000));

        let data = expiring_at.with(closure).unwrap();

        assert_eq!(data, 4);
    }

    #[test]
    fn test_with_expired() {
        let closure = |a: &u32| -> u32 {
            std::thread::sleep(Duration::from_millis(100));
            a * 2
        };
        let expiring_at = ExpiringAt::from_now(2, Duration::from_millis(50));

        let err = expiring_at.with(closure).unwrap_err();

        assert!(matches!(
            err,
            ResourceFailure::Latency(FailureMode::BudgetExceeded(..))
        ));
    }

    #[test]
    fn test_map() {
        let closure = |a: &u32| -> u32 { a * 3 };
        let at = Instant::now();
        let expiring_at = ExpiringAt::at(5, at);

        let mapped = expiring_at.map(closure);

        assert_eq!(mapped.at, at);
        assert_eq!(mapped.data, 15);
    }

    #[test]
    #[should_panic]
    fn test_map_panic() {
        let closure = |a: &u32| -> u32 {
            match a {
                0 => panic!("Cannot divide by 0"),
                _ => 100 / a,
            }
        };
        let expiring_at = ExpiringAt::at(0, Instant::now());

        expiring_at.map(closure);
    }
}
