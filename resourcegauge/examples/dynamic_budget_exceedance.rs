#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(inline_const)]
#![feature(adt_const_params)]
#![feature(const_option)]

// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use resourcegauge::prelude::*;
use std::time::Duration;

#[resourcegauge(max_latency = "1s")]
fn f1() -> bool {
    with_latency! {
        let _ = f2();
    }
    std::thread::sleep(Duration::from_millis(10));
    true
}

#[resourcegauge(max_latency = "1s")]
fn f2() -> bool {
    std::thread::sleep(Duration::from_millis(999));
    true
}

fn main() {
    let latency = ResourceBudget::<{ Duration::from_secs(1) }>::new();
    let _ = f1(latency).unwrap();
}
