#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(inline_const)]
#![feature(adt_const_params)]
#![feature(const_option)]

// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use resourcegauge::prelude::*;
use std::time::Duration;

#[resourcegauge(max_latency = "1s")]
fn f1() -> bool {
    with_latency! {
        let _ = f2();
    }
    check!();
    std::thread::sleep(Duration::from_millis(900));
    true
}

#[resourcegauge(max_latency = "1s")]
fn f2() -> bool {
    std::thread::sleep(Duration::from_millis(500));
    true
}

fn main() {
    for _ in 0..5 {
        let latency = ResourceBudget::<{ Duration::from_secs(1) }>::new();
        let _r = match f1(latency) {
            Ok((r, _)) => r,
            Err(e) => {
                dbg!(e);
                false
            }
        };
    }
}
