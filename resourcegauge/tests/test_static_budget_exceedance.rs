// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use quote::quote;
use std::env::current_dir;
use std::fs::{remove_file, File};
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;

#[test]
fn test_static_budget_exceedance_compiler_error() {
    let code = quote! {
        #![allow(incomplete_features)]
        #![feature(generic_const_exprs)]
        #![feature(inline_const)]
        #![feature(adt_const_params)]
        #![feature(const_option)]

        use resourcegauge::prelude::*;

        #[resourcegauge(max_latency = "1s")]
        fn f1() -> bool {
            with_latency! {
                let _ = f2();
            }
            // f2 already used up all our max_latency
            with_latency! {
                let _ = f2();
            }
            true
        }

        #[resourcegauge(max_latency = "1s")]
        fn f2() -> bool {
            true
        }

        fn main() {
            let latency = ResourceBudget::<{ std::time::Duration::from_secs(1) }>::new();
            let _ = f1(latency).unwrap();
        }
    };
    let mut path = current_dir().expect("Unable to get current_dir.");
    path.push("examples/static_budget_exceedance.rs");
    create_file_with_content(&path, &code.to_string());

    let output = match Command::new("cargo")
        .arg("run")
        .arg("--package")
        .arg("resourcegauge")
        .arg("--example")
        .arg("static_budget_exceedance")
        .output()
    {
        Ok(out) => {
            delete_file(&path);
            out
        }
        Err(e) => {
            delete_file(&path);
            panic!("Execution of cargo failed: {:?}", e);
        }
    };

    assert_eq!(output.status.code(), Some(101));
    assert!(String::from_utf8_lossy(&output.stderr).contains(
        "evaluation of `f1::<{ std :: time :: Duration :: from_secs (1) }>::{constant#1}` failed"
    ));
}

fn create_file_with_content(path: &PathBuf, content: &str) {
    let file = File::create(path).expect("File creation needs to be possible.");
    match write!(&file, "{}", content) {
        Ok(_) => {}
        Err(e) => {
            delete_file(path);
            panic!("Writing to file failed: {:?}", e)
        }
    }
}

fn delete_file(path: &PathBuf) {
    match remove_file(path) {
        Ok(_) => {}
        Err(e) => {
            panic!("Could not delete file: {}", e);
        }
    }
}
