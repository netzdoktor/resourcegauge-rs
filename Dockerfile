# SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
# SPDX-License-Identifier: CC0-1.0

FROM debian:bullseye-slim

RUN apt update && apt install -y build-essential curl make libssl-dev pkg-config zip

# Rustup
ENV RUSTUP_HOME=/opt/rustup
ENV CARGO_HOME=/opt/cargo
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="${PATH}:/opt/cargo/bin"

# Rust Toolchain
ENV RUST_TOOLCHAIN="stable"
RUN rustup toolchain install "${RUST_TOOLCHAIN}"
RUN rustup toolchain install "nightly"
RUN rustup default "${RUST_TOOLCHAIN}"

# Rust Tools
RUN rustup component add clippy
RUN rustup component add rustfmt
RUN cargo install cargo-about \
    cargo-outdated \
    cargo-udeps \
    cargo-tarpaulin

# Cleanup
RUN rm -rf /var/lib/apt/lists/*
