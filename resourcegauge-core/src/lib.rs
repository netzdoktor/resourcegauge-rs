// SPDX-FileCopyrightText: 2023 Andreas Schmidt <andreas.schmidt@cs.uni-saarland.de>
//
// SPDX-License-Identifier: MIT

use darling::FromMeta;
use proc_macro2::TokenStream;
use proc_macro_error::abort;
use quote::{format_ident, quote, ToTokens};
use syn::{fold::Fold, parse_quote, spanned::Spanned, ItemFn};

#[allow(clippy::unwrap_used)]
fn convert_signature(signature: syn::Signature, functions: Vec<Vec<syn::Ident>>) -> syn::Signature {
    // output
    let ty = if let syn::ReturnType::Type(_, ty) = signature.output {
        ty
    } else {
        parse_quote!(())
    };
    let latency_ident = format_ident!("LATENCY_{}", signature.ident);
    let output: syn::ReturnType = parse_quote! { -> Result<(#ty, resourcegauge::ResourceBudget<{ LATENCY.checked_sub(#latency_ident).unwrap() }>), resourcegauge::ResourceFailure> };

    // generics
    let mut params = signature.generics.params;
    params.push(parse_quote! { const LATENCY: std::time::Duration });

    let mut where_clause = signature
        .generics
        .where_clause
        .unwrap_or_else(|| parse_quote!(where));

    where_clause.predicates.extend(
        functions
            .into_iter()
            .map(|function| -> syn::WherePredicate {
                let ty = function.into_iter().fold(quote! { LATENCY }, |ty, l| {
                    let l = format_ident!("LATENCY_{}", l);
                    quote! { #ty.checked_sub(#l).unwrap() }
                });
                parse_quote! { ResourceBudget<{ #ty }>: }
            }),
    );

    let generics = syn::Generics {
        lt_token: parse_quote!(<),
        gt_token: parse_quote!(>),
        params,
        where_clause: Some(where_clause),
    };

    // inputs
    let latency = parse_quote! { latency: resourcegauge::ResourceBudget<LATENCY> };
    let mut inputs = signature.inputs;
    inputs.insert(0, latency);

    syn::Signature {
        output,
        generics,
        inputs,
        ..signature
    }
}

fn text_to_duration_tokens(span: proc_macro2::Span, text: &str) -> proc_macro2::TokenStream {
    // TODO: support more durations
    if text.ends_with('s') {
        let number = text.replace('s', "");
        let number: u64 = match number.parse() {
            Ok(number) => number,
            Err(_) => {
                abort!(span, "max_latency must have a number of seconds, e.g. '2s'")
            }
        };
        quote!(std::time::Duration::from_secs(#number))
    } else {
        abort!(span, "max_latency only supports seconds")
    }
}

#[derive(Default)]
struct EnumerateCheckBlocks {
    counter: usize,
}

impl syn::fold::Fold for EnumerateCheckBlocks {
    fn fold_expr_macro(&mut self, mut mac: syn::ExprMacro) -> syn::ExprMacro {
        match mac.mac.path.to_token_stream().to_string().as_str() {
            "check" => {
                let index = self.counter;
                self.counter += 1;
                mac.mac.tokens = quote!(#index);
                mac
            }
            "remaining_latency" | "next_deadline" => {
                let index = self.counter;
                mac.mac.tokens = quote!(#index);
                mac
            }
            _ => syn::ExprMacro {
                mac: self.fold_macro(mac.mac),
                ..mac
            },
        }
    }
}

fn enumerate_checks(input: &ItemFn) -> (proc_macro2::TokenStream, usize) {
    let mut enumerate = EnumerateCheckBlocks::default();
    let block = enumerate.fold_block(*input.block.clone());
    (block.to_token_stream(), enumerate.counter + 1)
}

struct WrapReturns<'a> {
    latency_ident: &'a syn::Ident,
}

impl<'a> WrapReturns<'a> {
    fn new(latency_ident: &'a syn::Ident) -> Self {
        Self { latency_ident }
    }
}

impl<'a> syn::fold::Fold for WrapReturns<'a> {
    fn fold_expr_closure(&mut self, closure: syn::ExprClosure) -> syn::ExprClosure {
        // TODO: keep on folding on other elements of the macro?
        // stop folding closures
        closure
    }

    #[allow(clippy::unwrap_used, clippy::expect_used)]
    fn fold_expr_return(&mut self, expr: syn::ExprReturn) -> syn::ExprReturn {
        // TODO: keep on folding on other elements of the macro?
        let expr = self.fold_expr(*expr.expr.expect("Must have expression"));
        let latency_ident = &self.latency_ident;
        parse_quote!(return {
            let result = #expr;
            unsafe {
                let mut m = RESOURCEGAUGE_MEASUREMENTS_LOCAL.load();
                m[m.len()-1] = RESOURCEGAUGE_START.elapsed();
                RESOURCEGAUGE_MEASUREMENTS_LOCAL.store(m);
            }; // TODO: EWMA / MAX instead of replace
            if std::time::Instant::now() > RESOURCEGAUGE_DEADLINE {
                Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(std::time::Instant::now() -  RESOURCEGAUGE_DEADLINE)))
            } else {
                Ok((result, latency.shorten::<{ LATENCY.checked_sub(#latency_ident).unwrap() }>()))
            }
        })
    }
}

struct Idents {
    measurements_ident: syn::Ident,
    latency_ident: syn::Ident,
}
impl Idents {
    fn new(ident: &proc_macro2::Ident) -> Idents {
        Self {
            measurements_ident: format_ident!("RESOURCEGAUGE_MEASUREMENTS_{}", ident),
            latency_ident: format_ident!("LATENCY_{}", ident),
        }
    }
}

#[allow(clippy::panic, clippy::expect_used, clippy::unwrap_used)]
fn process_block(block: TokenStream, idents: &Idents) -> syn::Block {
    let Idents { latency_ident, .. } = idents;
    let block: syn::Expr = syn::parse2(block).unwrap();
    let block = if let syn::Expr::Block(block) = block {
        let syn::Block { stmts, .. } = block.block;
        let (ret, head) = stmts
            .split_last()
            .expect("Block should have at least one statement");
        parse_quote!({
            #(#head)*
            return { #ret }
        })
    } else {
        panic!("Must have an expression.");
    };

    WrapReturns::new(latency_ident).fold_block(block)
}

#[derive(Default)]
struct CollectFunctions {
    functions: Vec<Vec<syn::Ident>>,
}

impl syn::fold::Fold for CollectFunctions {
    #[allow(clippy::unwrap_used, clippy::panic)]
    fn fold_item_macro(&mut self, mac: syn::ItemMacro) -> syn::ItemMacro {
        // TODO: keep on folding on other elements of the macro?
        if mac.mac.path == syn::Path::from_string("with_latency").unwrap() {
            let stmt: syn::Stmt = syn::parse2(mac.mac.tokens.clone()).unwrap();
            if let syn::Stmt::Local(stmt) = stmt {
                let (_, init) = match stmt.init {
                    Some(init) => init,
                    None => {
                        panic!()
                    }
                };
                let expr = *init;
                if let syn::Expr::Call(call) = expr {
                    let ident = format_ident!("{}", call.func.to_token_stream().to_string());
                    let mut prefix = self.functions.last().unwrap_or(&vec![]).clone();
                    prefix.push(ident);
                    self.functions.push(prefix);
                }
            }
        }
        mac
    }
}

fn collect_functions(input: &ItemFn) -> Vec<Vec<syn::Ident>> {
    let mut c = CollectFunctions::default();
    c.fold_block(*input.block.clone());
    c.functions
}

#[derive(Debug, FromMeta)]
struct MacroArgs {
    max_latency: String,
}

pub fn main_macro(
    args: Vec<syn::NestedMeta>,
    args_span: proc_macro2::Span,
    input: TokenStream,
) -> TokenStream {
    let args = match MacroArgs::from_list(&args) {
        Ok(v) => v,
        Err(e) => {
            return e.write_errors();
        }
    };

    let span = input.span();
    let input: ItemFn = match syn::parse2(input) {
        Ok(input) => input,
        Err(_) => {
            abort!(span, "#[resourcegauge] must be used on a function.")
        }
    };

    let latency = text_to_duration_tokens(args_span, &args.max_latency);

    let functions = collect_functions(&input);

    let input = ItemFn {
        sig: convert_signature(input.sig, functions),
        ..input
    };

    let idents = Idents::new(&input.sig.ident);

    // TODO: std_rewire: replace *_timeout() with a timeout-aware call.
    let (block, count) = enumerate_checks(&input);
    let block = process_block(block, &idents);

    let durations = std::iter::repeat(quote!(std::time::Duration::from_secs(0)))
        .take(count)
        .collect::<Vec<_>>();

    let Idents {
        measurements_ident,
        latency_ident,
    } = idents;
    let sig = input.sig;
    quote!(
        #[allow(non_upper_case_globals)]
        const #latency_ident: std::time::Duration = #latency;

        #[allow(non_upper_case_globals)]
        static mut #measurements_ident : crossbeam::atomic::AtomicCell<[std::time::Duration; #count]>
            = crossbeam::atomic::AtomicCell::new([#(#durations),*]);

        #sig {
            let RESOURCEGAUGE_MEASUREMENTS_LOCAL = unsafe { &#measurements_ident };

            let RESOURCEGAUGE_START = std::time::Instant::now();
            let RESOURCEGAUGE_DEADLINE = RESOURCEGAUGE_START + #latency;
            #block
        }

    )
}

pub fn with_latency(input: TokenStream) -> TokenStream {
    let span = input.span();
    let stmt: syn::Stmt = match syn::parse2(input) {
        Ok(stmt) => stmt,
        Err(_) => {
            abort!(span, "with_latency! must wrap a statement.")
        }
    };
    if let syn::Stmt::Local(stmt) = stmt {
        let pat = stmt.pat;
        let (_, init) = match stmt.init {
            Some(init) => init,
            None => {
                abort!(span, "with_latency! must wrap a statement with init.")
            }
        };
        let expr = *init;
        if let syn::Expr::Call(mut call) = expr {
            call.args.insert(0, parse_quote!(latency));
            quote!(
                let (#pat, latency) = #call?;
            )
        } else {
            abort!(
                span,
                "with_latency! must wrap a 'let _ = call(...);' statement."
            )
        }
    } else {
        abort!(
            span,
            "with_latency! must wrap a 'let _ = call(...);' statement."
        )
    }
}

pub fn remaining_latency(input: TokenStream) -> TokenStream {
    let next_deadline = next_deadline(input);
    quote! {
       #next_deadline.duration_since(std::time::Instant::now())
    }
}

#[allow(clippy::unwrap_used)]
pub fn next_deadline(input: TokenStream) -> TokenStream {
    let index: syn::Lit = syn::parse2(input).unwrap();
    quote! {
       unsafe {
           let m = RESOURCEGAUGE_MEASUREMENTS_LOCAL.load();
           let next = m[#index];
           let end = m[m.len()-1];
           RESOURCEGAUGE_DEADLINE.checked_sub(end.saturating_sub(next)).unwrap_or(std::time::Instant::now())
       }
    }
}

#[allow(clippy::unwrap_used)]
pub fn check(input: TokenStream) -> TokenStream {
    let index: syn::Lit = syn::parse2(input).unwrap();

    quote!({
        let remaining_latency = unsafe {
            let mut m = RESOURCEGAUGE_MEASUREMENTS_LOCAL.load();
            m[#index] = RESOURCEGAUGE_START.elapsed();
            RESOURCEGAUGE_MEASUREMENTS_LOCAL.store(m);
            m[m.len()-1].saturating_sub(m[#index])
        };
        let now = std::time::Instant::now();
        if now > RESOURCEGAUGE_DEADLINE {
            return Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(now - RESOURCEGAUGE_DEADLINE)));
        }
        if now + remaining_latency > RESOURCEGAUGE_DEADLINE {
            return Err(ResourceFailure::Latency(FailureMode::BudgetExceedanceExpected(RESOURCEGAUGE_DEADLINE - now)));
        }
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_tokens_eq::assert_tokens_eq;
    use quote::{quote, ToTokens};

    #[test]
    fn test_with_latency() {
        let input: syn::Stmt = parse_quote! {
            let (x,y) = foo(b);
        };
        let result = with_latency(input.to_token_stream());
        let expected = quote! {
            let ((x,y), latency) = foo(latency, b)?;
        };
        assert_tokens_eq!(expected, result);
    }

    #[test]
    fn test_convert() {
        let input: syn::Signature = parse_quote! (
            fn foo(input: In) -> Output
        );
        let result = convert_signature(input, vec![]);
        let expected: syn::ItemFn = parse_quote!(
            fn foo<const LATENCY: std::time::Duration>(
                latency: resourcegauge::ResourceBudget<LATENCY>,
                input: In,
            ) -> Result<
                (
                    Output,
                    resourcegauge::ResourceBudget<{ LATENCY.checked_sub(LATENCY_foo).unwrap() }>,
                ),
                resourcegauge::ResourceFailure,
            > {
            }
        );
        assert_tokens_eq!(
            expected.to_token_stream(),
            quote! (#result { }).to_token_stream() // assert only work on ItemFn, not on Signature
        );
    }

    #[test]
    fn test_main_macro() {
        let input = quote! {
            fn foo() -> () {
                ()
            }
        };
        let args_span = input.span();
        let args = vec![parse_quote!(max_latency = "2s")];
        let result = main_macro(args, args_span, input);
        let expected = quote! {
            #[allow(non_upper_case_globals)]
            const LATENCY_foo: std::time::Duration = std::time::Duration::from_secs(2u64);
            #[allow(non_upper_case_globals)]
            static mut RESOURCEGAUGE_MEASUREMENTS_foo: crossbeam::atomic::AtomicCell<[std::time::Duration; 1usize]> =
                crossbeam::atomic::AtomicCell::new([std::time::Duration::from_secs(0)]);
            fn foo<const LATENCY: std::time::Duration>(latency: resourcegauge::ResourceBudget<LATENCY>) -> Result<((), resourcegauge::ResourceBudget<{ LATENCY.checked_sub(LATENCY_foo).unwrap() }>), resourcegauge::ResourceFailure> {
                let RESOURCEGAUGE_MEASUREMENTS_LOCAL = unsafe { &RESOURCEGAUGE_MEASUREMENTS_foo };
                let RESOURCEGAUGE_START = std::time::Instant::now();
                let RESOURCEGAUGE_DEADLINE = RESOURCEGAUGE_START + std::time::Duration::from_secs(2u64);
                {
                    return {
                        let result = { () };
                        unsafe {
                            let mut m = RESOURCEGAUGE_MEASUREMENTS_LOCAL.load();
                            m[m.len()-1] = RESOURCEGAUGE_START.elapsed();
                            RESOURCEGAUGE_MEASUREMENTS_LOCAL.store(m);
                        };
                        if std::time::Instant::now() > RESOURCEGAUGE_DEADLINE {
                            Err(ResourceFailure::Latency(FailureMode::BudgetExceeded(std::time::Instant::now() - RESOURCEGAUGE_DEADLINE)))
                        } else {
                            Ok((result, latency.shorten::<{LATENCY.checked_sub(LATENCY_foo).unwrap()}>()))
                        }
                    }
                }
            }
        };
        assert_tokens_eq!(expected, result.to_token_stream());
    }

    macro_rules! text_to_duration_tests {
        ($suite:ident, $($name:ident: $input:expr, $output:expr,)*) => {
            mod $suite {
                use super::*;
                $(
                    #[test]
                    fn $name() -> () {
                        let tokens = text_to_duration_tokens(proc_macro2::Span::call_site(), $input);
                        assert_tokens_eq!($output, tokens);
                    }
                )*
            }
        };
    }
    text_to_duration_tests!(simple,
        one_sec: "1s", quote!(std::time::Duration::from_secs(1u64)),
        two_sec: "2s", quote!(std::time::Duration::from_secs(2u64)),
    );
}
